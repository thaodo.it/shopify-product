require('dotenv').config();

const express = require("express");
const app = express();

const bodyParser = require("body-parser");
const cors = require("cors");

const get_products = require("./requests/get_products");
const create_product = require("./requests/create_product");

const port = process.env.PORT || 8080;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
    next();
});

app.post("/create_product", (req, res) => {
    let API_KEY = req.headers['api_key'] || '';
    let SECRET_KEY = req.headers['secret_key'] || '';
    let SHOPIFY_SITE = req.headers['shopify_site'] || '';
    if (API_KEY === '' ) {
        res.status(400).send('API_KEY header not be empty');
    }

    if (SECRET_KEY === '') {
        res.status(400).send('SECRET_KEY header not be empty');
    }

    if (SHOPIFY_SITE === '') {
        res.status(400).send('SHOPIFY_SITE header not be empty');
    }

    create_product.create_product(API_KEY, SECRET_KEY, SHOPIFY_SITE, req.body)
        .then((json) => {
            res.status(200).send(json);
        });
});

app.get("/get_products", (req, res) => {
    let API_KEY = req.headers['api_key'] || '';
    let SECRET_KEY = req.headers['secret_key'] || '';
    let SHOPIFY_SITE = req.headers['shopify_site'] || '';
    if (API_KEY === '' ) {
        res.status(400).send('API_KEY header not be empty');
    }

    if (SECRET_KEY === '') {
        res.status(400).send('SECRET_KEY header not be empty');
    }

    if (SHOPIFY_SITE === '') {
        res.status(400).send('SHOPIFY_SITE header not be empty');
    }

    get_products.get_products(API_KEY, SECRET_KEY, SHOPIFY_SITE)
        .then((json) => {
            res.status(200).send(json);
        });
});

app.listen(port, () => {
    console.log(`Server running on port ${ port }`);
});