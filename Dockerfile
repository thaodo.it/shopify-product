FROM alpine:3.12

# Update & install required packages
RUN apk add --update nodejs npm bash git

# Install app dependencies
COPY package.json /www/package.json
RUN cd /www; npm install; npm install pm2 -g

# Copy app source
COPY . /www

# Set work directory to /www
WORKDIR /www

RUN mv /www/.env.example /www/.env
# set your port
#ENV PORT 8080

# expose the port to outside world
EXPOSE 9000

# start command as per package.json
CMD ["pm2-runtime", "server.js"]