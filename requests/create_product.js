const fetch = require('node-fetch');

const create_product = async (api_key, secret_key, shopify_site, body) => {
    return await fetch(`https://${ api_key }:${ secret_key }@${ shopify_site }.myshopify.com/admin/api/2020-04/products.json`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        }
    )
    .then((res) => res.json())
    .then((json) => json);
};

exports.create_product = create_product;