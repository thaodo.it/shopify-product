# shopify-product



```
curl --location --request POST 'http://localhost:8080/create_product' \
--header 'api_key: a4592b8583b586b4a3de22d09f0f8546' \
--header 'secret_key: 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' \
--header 'shopify_site: psa-product' \
--header 'Content-Type: application/json' \
--data-raw '{
    "product": {
        "id": 6090740695212,
        "title": "2020 Annual Events - Doormat",
        "body_html": "<p><strong>Doormat</strong> is the first glimpse of your home new visitors will see and you’ll definitely want to make a good impression. On top of that, the doormat is your home’s first line of defense against all the debris. As well as also being an easy way to instantly make your entryway look organized and pulled together. These accessories also serve as a decoration that welcomes all your guests to your home with this warm and unique pattern.</p>\n<h3><span style=\"color: #ff2a00;\"><strong>Product features:</strong></span></h3>\n<ul>\n<li>Mats can be highly effective in trapping dirt before entering the house and reducing the amount of dust and dirt that comes into the building.</li>\n<li>0.25″ slim, making it just the right size for every entryway. Our unique doormats make a beautiful addition to your entryways, craft rooms, and office spaces.</li>\n<li>Perfect for any deck, patio, porch, veranda, and entryway and extend a warm welcome to all who enter your home!</li>\n<li>Backed with non-slip material and topped with polyester felt that is low-profile to reduce chances of tripping.</li>\n<li>UV protection with high-quality print will help slow the fading process in high-sunlit areas.</li>\n<li>This mat is safe for indoor or outdoor use.</li>\n</ul>\n<h3><span style=\"color: #ff2a00;\">Package includes:</span></h3>\n<ul>\n<li>1 * Doormat</li>\n</ul>",
        "vendor": "Personalize Them",
        "product_type": "",
        "created_at": "2020-11-21T07:49:57-07:00",
        "handle": "2020-annual-events-doormat",
        "updated_at": "2021-01-07T23:45:16-07:00",
        "published_at": null,
        "template_suffix": null,
        "published_scope": "web",
        "tags": "",
        "variants": [
            {
                "id": 37567158452396,
                "product_id": 6090740695212,
                "title": "SMALL (15.8\" X 23.6\")",
                "price": "29.99",
                "sku": null,
                "position": 1,
                "inventory_policy": "continue",
                "compare_at_price": "49.99",
                "fulfillment_service": "manual",
                "inventory_management": null,
                "option1": "SMALL (15.8\" X 23.6\")",
                "option2": null,
                "option3": null,
                "created_at": "2020-11-21T07:49:57-07:00",
                "updated_at": "2020-11-21T07:49:57-07:00",
                "taxable": true,
                "barcode": null,
                "grams": 0,
                "image_id": null,
                "weight": 0,
                "weight_unit": "lb",
                "inventory_item_id": 39660147540140,
                "inventory_quantity": 0,
                "old_inventory_quantity": 0,
                "requires_shipping": true
            },
            {
                "id": 37567158485164,
                "product_id": 6090740695212,
                "title": "MEDIUM (17.7\" X 29.5\")",
                "price": "34.99",
                "sku": null,
                "position": 2,
                "inventory_policy": "continue",
                "compare_at_price": "66.99",
                "fulfillment_service": "manual",
                "inventory_management": null,
                "option1": "MEDIUM (17.7\" X 29.5\")",
                "option2": null,
                "option3": null,
                "created_at": "2020-11-21T07:49:57-07:00",
                "updated_at": "2020-11-21T07:49:57-07:00",
                "taxable": true,
                "barcode": null,
                "grams": 0,
                "image_id": null,
                "weight": 0,
                "weight_unit": "lb",
                "inventory_item_id": 39660147572908,
                "inventory_quantity": 0,
                "old_inventory_quantity": 0,
                "requires_shipping": true
            },
            {
                "id": 37567158517932,
                "product_id": 6090740695212,
                "title": "LARGE (23.6\" X 35.4\")",
                "price": "44.99",
                "sku": null,
                "position": 3,
                "inventory_policy": "continue",
                "compare_at_price": "79.99",
                "fulfillment_service": "manual",
                "inventory_management": null,
                "option1": "LARGE (23.6\" X 35.4\")",
                "option2": null,
                "option3": null,
                "created_at": "2020-11-21T07:49:57-07:00",
                "updated_at": "2020-11-21T07:49:57-07:00",
                "taxable": true,
                "barcode": null,
                "grams": 0,
                "image_id": null,
                "weight": 0,
                "weight_unit": "lb",
                "inventory_item_id": 39660147605676,
                "inventory_quantity": 0,
                "old_inventory_quantity": 0,
                "requires_shipping": true
            }
        ],
        "options": [
            {
                "id": 7772903473324,
                "product_id": 6090740695212,
                "name": "Size",
                "position": 1,
                "values": [
                    "SMALL (15.8\" X 23.6\")",
                    "MEDIUM (17.7\" X 29.5\")",
                    "LARGE (23.6\" X 35.4\")"
                ]
            }
        ],
        "images": [
            {
                "id": 22824714010796,
                "product_id": 6090740695212,
                "position": 1,
                "created_at": "2020-11-21T07:49:57-07:00",
                "updated_at": "2020-11-21T07:50:01-07:00",
                "alt": "2020 Annual Events - Doormat - Personalize Them",
                "width": 1296,
                "height": 1296,
                "src": "https://cdn.shopify.com/s/files/1/0511/4524/4844/products/1605687600592e84222e.jpg?v=1605970201",
                "variant_ids": []
            },
            {
                "id": 22824714043564,
                "product_id": 6090740695212,
                "position": 2,
                "created_at": "2020-11-21T07:49:57-07:00",
                "updated_at": "2020-11-21T07:50:01-07:00",
                "alt": "2020 Annual Events - Doormat - Personalize Them",
                "width": 1296,
                "height": 1296,
                "src": "https://cdn.shopify.com/s/files/1/0511/4524/4844/products/1605687600fd026e3c9e.jpg?v=1605970201",
                "variant_ids": []
            },
            {
                "id": 22824714076332,
                "product_id": 6090740695212,
                "position": 3,
                "created_at": "2020-11-21T07:49:57-07:00",
                "updated_at": "2020-11-21T07:50:01-07:00",
                "alt": "2020 Annual Events - Doormat - Personalize Them",
                "width": 1296,
                "height": 1296,
                "src": "https://cdn.shopify.com/s/files/1/0511/4524/4844/products/1605687601b250868849.jpg?v=1605970201",
                "variant_ids": []
            }
        ],
        "image": {
            "id": 22824714010796,
            "product_id": 6090740695212,
            "position": 1,
            "created_at": "2020-11-21T07:49:57-07:00",
            "updated_at": "2020-11-21T07:50:01-07:00",
            "alt": "2020 Annual Events - Doormat - Personalize Them",
            "width": 1296,
            "height": 1296,
            "src": "https://cdn.shopify.com/s/files/1/0511/4524/4844/products/1605687600592e84222e.jpg?v=1605970201",
            "variant_ids": []
        }
    }
}
```
